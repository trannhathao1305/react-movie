import { message } from "antd";
import { postLogin } from "../../service/userService";
import { SET_USER_LOGIN } from "../constant/userConstant";

export const setUserAction = (payload) => {
  return {
    type: SET_USER_LOGIN,
    payload,
  };
};
// values thông tin từ form
export const setUserActionService = (values, onSuccess) => {
  return (dispatch) => {
    postLogin(values)
      .then((res) => {
        message.success("Đăng nhập thành công");
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.error("Đăng nhập thất bại");
      });
  };
};
