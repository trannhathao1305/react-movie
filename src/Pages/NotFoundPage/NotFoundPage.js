import React from "react";

export default function NotFoundPage() {
  return (
    <div className="h-screen w-screen flex justify-center items-center">
      <h1 className="text-center text-5xl font-black text-red-600 animate-bounce">
        404
      </h1>
    </div>
  );
}
