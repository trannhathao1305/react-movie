import React from "react";
import { useParams } from "react-router-dom";

export default function DetailPage() {
  let params = useParams();
  console.log(params);
  return (
    <div>
      <h2 className="text-red-600 text-center font-black">
        Mã phim: {params.id}
      </h2>
    </div>
  );
}
